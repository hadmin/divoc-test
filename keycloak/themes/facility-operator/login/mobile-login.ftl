<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
    <#if section = "title">
        ${msg("loginTitle",(realm.displayName!''))}
    <#elseif section = "header">
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet"/>
        <link href="${url.resourcesPath}/img/favicon.png" rel="icon"/>
        <script>
            window.onload = function(e){
                document.getElementById("mobile_number").addEventListener("change", function(evt) {
                    console.log(evt.target.value)
                    sessionStorage.setItem("mobile_number", evt.target.value)
                })
                if(window.location.protocol === "https:") {
                    let formField = document.getElementById("kc-form-login");
                    if (formField) {
                        formField.action = formField.action.replace("http","https");
                    }
                }
            }
            function CallWebAPI() {
                console.log("web api")
                // New XMLHTTPRequest
                var user = document.getElementById("mobile_number").value;
                var pass = document.getElementById("password").value;
                var request = new XMLHttpRequest();
                request.open("POST", " https://api.mbnext.dev.k8s.sandboxaddis.com/api/Auth/authenticate", false);
                request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                request.send(JSON.stringify({ "userName": user, "password": pass }));
                // view request status
                alert(request.status);
                request.onload = () => console.log("response" + xhr.response);
            }
        </script>
    <#elseif section = "form">

        <div class="box-container">
            <#if realm.password>
                <div>
                    <form id="kc-form-login" class="form" onsubmit="login.disabled = true; return true;"
                          action="${url.loginAction}" method="post">
                        <div class="input-wrapper">
                            <div class="input-field mobile">
                                <img class="mobile-logo"/>
<#--                                <label for="mobile_number" class="mobile-prefix">+91</label>-->
                                <input id="mobile_number" class="login-field" placeholder="XXXXXXXXXX"
                                       type="text"
                                       name="mobile_number" autofocus
                                       tabindex="1"/>
                            </div>
                            <div class="input-field">
<#--                                <label for="mobile_number" class="mobile-prefix">+91</label>-->
                                <input id="password" class="login-field" placeholder="password"
                                       type="password"
                                       name="password"/>
                            </div>

                        </div>
                        <input type="hidden" id="type-hidden-input" name="form_type" value="login"/>
                        <button id="submit-btn" class="submit" type="submit" tabindex="3" onclick="CallWebAPI()">
                            <span>GET OTP</span>
                        </button>
                    </form>
                </div>
            </#if>


        </div>
    </#if>
</@layout.registrationLayout>
