import {appIndexDb} from "../AppDatabase";

const AUTHORIZE = "/divoc/api/v1/authorize"
const PRE_ENROLLMENT = "/divoc/api/v1/preEnrollments"
const PROGRAMS = "/divoc/api/v1/programs/current"
const VACCINATORS = "/divoc/admin/api/v1/vaccinators"
const VACCINES = "https://api.mbnext.dev.k8s.sandboxaddis.com/api/Item/GetAll"
const BATCHES = "https://api.mbnext.dev.k8s.sandboxaddis.com/api/Batch/GetBatchByItemId"
const CERTIFY = "/divoc/api/v1/certify"
const USER_INFO = "/divoc/api/v1/users/me"
const FACILITY_DETAILS = "/divoc/admin/api/v1/facility";
const FLAGR_APPLICATION_CONFIG = "/config/api/v1/evaluation";
const FACILITY_ID = "FACILITY_ID"
const ENROLLMENT_ID = "ENROLLMENT_ID"
const PROGRAM_ID = "PROGRAM_ID"
const FACILITY_SLOTS = `/divoc/admin/api/v1/facility/${FACILITY_ID}/schedule`
const ENROLLMENT_BY_CODE = `/divoc/api/v1/preEnrollments/${ENROLLMENT_ID}`
const VERIFY_CERTIFICATE = "/divoc/api/v1/certificate/revoked"

export class ApiServices {

    static async login(mobileNumber, otp) {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json', 'accept': 'application/json'},
            body: JSON.stringify({mobile: mobileNumber, token2fa: otp})
        };
        return fetch(AUTHORIZE, requestOptions)
            .then(response => response.json())
    }

    static async fetchPreEnrollments(date=new Date()) {
        const qParams = new URLSearchParams();
        qParams.set("date", date.toISOString().slice(0, 10));
        const requestOptions = {
            method: 'GET',
            headers: {'accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem("token")}
        };
        return fetch(PRE_ENROLLMENT + `?${qParams.toString()}`, requestOptions)
            .then(response => response.json())
    }

    static async fetchEnrollmentByCode(enrollCode) {
        const requestOptions = {
            method: 'GET',
            headers: {'accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem("token")}
        };
        const apiURL = ENROLLMENT_BY_CODE.replace(ENROLLMENT_ID, enrollCode)
        return fetch(apiURL, requestOptions)
            .then(response => response.json())
    }

    static async fetchPrograms() {
        const requestOptions = {
            method: 'GET',
            headers: {'accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem("token")}
        };
        return fetch(PROGRAMS, requestOptions)
            .then(response => response.json())
    }


    static async fetchVaccinators() {
        const requestOptions = {
            method: 'GET',
            headers: {'accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem("token")}
        };
        return fetch(VACCINATORS, requestOptions)
            .then(response => response.json())
    }

    static async fetchVaccines() {
        const requestOptions = {
            method: 'GET',
            headers: {'accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem("token")}
        };
        return fetch(VACCINES, requestOptions)
            .then(response => response.json())
    }

    static async fetchBatches() {
        const requestOptions = {
            method: 'GET',
            headers: {'accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem("token")}
        };
        return fetch(BATCHES, requestOptions)
            .then(response => response.json())
    }

    static async certify(certifyPatients) {
        const userDetails = await appIndexDb.getUserDetails();
        const facilityDetails = userDetails.facilityDetails;
        const certifyBody = certifyPatients.map((item, index) => {
            const patientDetails = item.patient;
            return {
                meta: {
                    enrollmentOsid: patientDetails.osid ?? "",
                    registrationDate: patientDetails.registrationDate,
                    respiratoryRate: patientDetails.respiratoryRate,
                    temperature: patientDetails.temperature,
                    pulseRate: patientDetails.pulseRate,
                    systolicBloodPressure: patientDetails.systolicBloodPressure,
                    diastolicBloodPressure: patientDetails.diastolicBloodPressure,
                    occupation: patientDetails.occupation,
                    comorbidities: patientDetails.comorbidities,
                    relativeName: patientDetails.relativeName,
                    relativePhoneNumber: patientDetails.relativePhoneNumber,
                    pregnant: patientDetails.pregnant,
                    lactating: patientDetails.lactating,
                    height: patientDetails.height,
                    weight: patientDetails.weight,
                    onMedication: patientDetails.onMedication,
                    specifyOnMedication: patientDetails.specifyOnMedication,
                    knownAllergies: patientDetails.knownAllergies,
                    specifyKnownAlergies: patientDetails.specifyKnownAlergies,
                    vaccinesReceived: patientDetails.vaccinesReceived,
                    specifyVaccinesReceived: patientDetails.specifyVaccinesReceived,
                    substanceAbuse: patientDetails.substanceAbuse,
                    specifySubstanceAbuse: patientDetails.specifySubstanceAbuse,
                    consciousnessLevel: patientDetails.consciousnessLevel,
                    spoTwo: patientDetails.spoTwo,
                    // serialNumber: patientDetails.serialNumber,
                    // mrn: patientDetails.mrn,
                    // motherName: patientDetails.motherName,
                    // motherMrn: patientDetails.motherMrn,
                    // motherTTlastPregnancy: patientDetails.motherTTlastPregnancy,
                    // motherTTtotal: patientDetails.motherTTtotal,
                    // registrationDate: patientDetails.registrationDate,
                    // wereda: patientDetails.wereda,
                    // ketena: patientDetails.ketena,
                    // antigens: item.vaccination.antigens,
                    // immunizationCompleted: item.vaccination.immunizationCompleted,
                    // neonatalProtection: item.vaccination.neonatalProtection,
                    // weight: item.vaccination.weight,
                    // vitaminAreceived: item.vaccination.vitaminAreceived
                    // zScore: item.vaccination.weight - 5
                },                
                preEnrollmentCode: item.enrollCode,
                enrollmentType: patientDetails.enrollmentType,
                programId: item.programId,
                comorbidities: patientDetails.comorbidities ?? [],
                recipient: {
                    contact: [
                        "tel:" + patientDetails.phone,
                        "mailto:" + patientDetails.email
                    ],
                    age: "" + (new Date().getFullYear() - patientDetails.yob),
                    gender: patientDetails.gender,
                    identity: item.identity ?? "N/A",
                    name: patientDetails.name,
                    nationality: patientDetails.nationalId ? patientDetails.nationalId : "",

                    address: {
                        addressLine1: patientDetails.address.addressLine1 ?? "N/A",
                        addressLine2: patientDetails.address.addressLine2 ?? "N/A",
                        district: patientDetails.address.district ?? "N/A",
                        state: patientDetails.address.state ?? "N/A",
                        pincode: patientDetails.address.pincode ?? "N/A"
                    }
                },

                vaccination: {
                    batch: item.vaccination.batch,
                    date: item.vaccination.date,
                    effectiveStart: item.vaccination.effectiveStart,
                    effectiveUntil: item.vaccination.effectiveUntil ?? item.vaccination.effectiveStart,
                    manufacturer: item.vaccination.manufacturer ?? "N/A",
                    name: item.vaccination.name ?? "N/A",
                    dose: item.vaccination.dose ?? 1.0,
                    totalDoses: item.vaccination.totalDoses ?? 15.0,
                },
                vaccinator: {
                    name: item.vaccinatorName ?? "N/A",
                },
                facility: {
                    name: facilityDetails.facilityName ?? "N/A",
                    address: {
                        addressLine1: facilityDetails.address.addressLine1 ?? "N/A",
                        addressLine2: facilityDetails.address.addressLine2 ?? "N/A",
                        district: facilityDetails.address.district ?? "N/A",
                        state: facilityDetails.address.state ?? "N/A",
                        pincode: facilityDetails.address.pincode ?? "N/A"
                    }
                }
            }
        })

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("token")
            },
            body: JSON.stringify(certifyBody)
        };

        return fetch(CERTIFY, requestOptions)
            .then(response => {
                if (response.status === 200) {
                    return {}
                }
                return response.json()
            })
    }

    static async getUserDetails() {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("token")
            },
        };
        return fetch(USER_INFO, requestOptions)
            .then(response => {
                return response.json()
            })
    }

    static async getFacilityDetails() {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("token")
            },
        };
        return fetch(FACILITY_DETAILS, requestOptions)
            .then(response => {
                return response.json()
            })
    }

    static async fetchApplicationConfigFromFlagr() {
        const data = {
            "flagKey": "country_specific_features"
        };
        return this.fetchFlagrConfigs(data);
    }

    static fetchFlagrConfigs(data) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify(data)
        };
        return fetch(FLAGR_APPLICATION_CONFIG, requestOptions)
            .then(response => {
                return response.json()
            })
    }

    static async fetchFacilitySchedule(facilityId) {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("token")
            },
        };
        const apiURL = FACILITY_SLOTS
            .replace(FACILITY_ID, facilityId);

        return fetch(apiURL, requestOptions)
            .then(response => {
                return response.json()
            })
    }

    static async checkIfRevokedCertificate(data) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify(data)
        };
        return fetch(VERIFY_CERTIFICATE, requestOptions)
            .then(response => {
                return response
            }).catch((e) => {
                console.log(e);
                return e
            });
    }
}
