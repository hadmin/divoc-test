import {getSelectedProgram} from "../components/ProgramSelection";

export function saveVaccinationDetails(payload) {
    const vaccinationDetails = getVaccinationDetails() || {}
    vaccinationDetails.vaccinatorId = payload.vaccinatorId;
    vaccinationDetails.medicineId = payload.medicineId;
    vaccinationDetails.dose = payload.dose;
    vaccinationDetails.lastBatchId = payload.batchId
    if (vaccinationDetails.batchIds) {
        const batchIds = vaccinationDetails.batchIds
        if (!batchIds.includes(payload.batchId)) {
            batchIds.push(payload.batchId);
            vaccinationDetails.batchIds = batchIds;
        }
    } else {
        vaccinationDetails.batchIds = [payload.batchId]
    }
    const selectedProgram = getSelectedProgram()
    localStorage.setItem(selectedProgram + "_vaccination_details", JSON.stringify(vaccinationDetails))
}

export function saveVaccinationDetailsInfant(payload) {
    const vaccinationDetails = getVaccinationDetailsInfant() || {}
    vaccinationDetails.immunizationCompleted = payload.userImmunization.immunisationCompleted;
    vaccinationDetails.neonatalProtection = payload.userImmunization.pab;
    vaccinationDetails.weight = payload.weight;
    vaccinationDetails.vitaminAreceived = payload.vitaminAreceived;
    for (let key in payload.antigens) {
        if (payload.antigens.hasOwnProperty(key)) {
            vaccinationDetails.antigensReceived.push(key);;
        }
    }
    const selectedProgram = getSelectedProgram()
    localStorage.setItem(selectedProgram + "_vaccination_details", JSON.stringify(vaccinationDetails))
}


export function getVaccinationDetails() {
    const selectedProgram = getSelectedProgram()
    const vaccinationDetails = localStorage.getItem(selectedProgram + "_vaccination_details");
    if (!vaccinationDetails) {
        return {
            vaccinatorId: null,
            medicineId: null,
            dose: null,
            lastBatchId: null,
            batchIds: []
        }
    }
    return JSON.parse(vaccinationDetails)
}

export function getVaccinationDetailsInfant() {
    const selectedProgram = getSelectedProgram()
    const vaccinationDetails = localStorage.getItem(selectedProgram + "_vaccination_details");
    if (!vaccinationDetails) {
        return {
            antigensReceived: [],
            immunizationCompleted: null,
            neonatalProtection: null,
            weight: null,
            vitaminAreceived: null
        }
    }
    return JSON.parse(vaccinationDetails)
}
