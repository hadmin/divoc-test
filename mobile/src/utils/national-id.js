export function getNationalIdType(nationalId) {
  return nationalId.split(":")[1];
}

export function getNationalIdNumber(nationalId) {
  return nationalId.split(":")[2];
}

export function constuctNationalId(idtype, idNumber) {
  return ["did", idtype, idNumber].join(":");
}

export function getNameOfTheId(value) {
  return ID_TYPES.find((id) => id.value === value).name;
}

export const ID_TYPES = [
  {
    id: "kebeleId",
    name: "app.beneficiary.in.gov.kebele",
    value: "in.gov.kebele",
  },
  {
    id: "workId",
    name: "app.beneficiary.in.gov.workid",
    value: "in.gov.workid",
  },
  {
    id: "drivingLicense",
    name: "app.beneficiary.in.gov.drivinglicense",
    value: "in.gov.drivinglicense",
  },
  {
    id: "passport",
    name: "app.beneficiary.in.gov.passport",
    value: "in.gov.passport",
  },
  {
    id: "other",
    name: "app.beneficiary.in.gov.other",
    value: "in.gov.other",
  },
];
