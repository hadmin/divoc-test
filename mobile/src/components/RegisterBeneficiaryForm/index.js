import React, {useEffect, useRef, useState} from "react";
import "./index.css"
import {useSelector} from "react-redux";
import {constuctNationalId, getNationalIdNumber, getNationalIdType, ID_TYPES} from "../../utils/national-id";
import {BaseFormCard} from "../BaseFormCard";
import {getMessageComponent, LANGUAGE_KEYS} from "../../lang/LocaleContext";
import {maskPersonalDetails} from "../../utils/maskPersonalDetails";
import {CustomButton} from "../CustomButton";
import {
  ERROR_ID_MESSAGE,
  DISTRICT_ERROR_MSG,
  EMAIL_ERROR_MESSAGE,
  GENDER_ERROR_MSG,
  INVALID_NAME_ERR_MSG,
  MAXIMUM_LENGTH_OF_NAME_ERROR_MSG,
  MINIMUM_LENGTH_OF_NAME_ERROR_MSG,
  NAME_ERROR_MSG,
  NATIONAL_ID_ERROR_MSG,
  NATIONAL_ID_TYPE_ERROR_MSG,
  NATIONALITY_ERROR_MSG,
  PINCODE_ERROR_MESSAGE,
  STATE_ERROR_MSG,
} from "./error-constants";
import {isInValidAadhaarNumber, isValidName, isValidPincode} from "../../utils/validations";
import {applicationConfigsDB} from "../../Services/ApplicationConfigsDB";
import {getSelectedProgram, getSelectedProgramId} from "../ProgramSelection";
import {programDb} from "../../Services/ProgramDB";
import doseCompletedImg from "../../assets/img/dose-completed.svg";
import currentDoseImg from "../../assets/img/dose-currentdose.svg";
import nextDoseImg from "../../assets/img/dose-nextdose.svg";
import {DosesState} from "../DosesState";
import {
  FormattedMessage,
  FormattedNumber,
  IntlProvider,
  useIntl,
} from "react-intl";

const GENDERS = [
  "male",
  "female",
];
const OCCUPATIONS = [
  "health",
  "nonHealth",
];
const NATIONALITY = [
  "ethiopian",
  "nationalityOther",
];
const COMORBIDITIES = [
  "Heart Disease",
  "Lung Disease",
  "CNS Disease",
  "Skin Disease",
  "Other",
];



export const RegisterBeneficiaryForm = ({
  verifyDetails,
  state,
  onBack,
  onContinue,
  buttonText,
}) => {
  const [formData, setFormData] = useState({ ...state });

  return (
    <div className="new-enroll-container">
      <BaseFormCard
        title={getMessageComponent(LANGUAGE_KEYS.ENROLLMENT_TITLE)}
        onBack={
          verifyDetails
            ? () => {
                onBack(formData);
              }
            : undefined
        }
      >
        <BeneficiaryForm
          verifyDetails={verifyDetails}
          state={state}
          onContinue={onContinue}
          buttonText={buttonText}
        />
      </BaseFormCard>
    </div>
  );
};

export function BeneficiaryForm({
  verifyDetails,
  state,
  onContinue,
  buttonText,
}) {
  const walkInForm = useRef(null);

  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({ ...state });

  useEffect(() => {
    walkInForm.current.scrollIntoView();
  }, [verifyDetails]);

  function setValue(evt) {
    console.log(evt)
    setFormData((state) => ({
      ...state,
      [evt.target.name]: evt.target.value,
    }));
  }

  function setIdValue(val){
    console.log(val)
    setFormData((state) => ({
      ...state,
      "identity": val,
    }));
  }

  function setRadioValue(evt) {
    if(evt.target.value == "yes"){
      setFormData((state) => ({
        ...state,
        [evt.target.name]: true,
      }));
    }
  }

  function setCheckBoxValue(evt) {
    if(evt.target.checked){
      setFormData((state) => ({
        ...state,
        [evt.target.name]: true,
      }));
    }
  }

  function setComorbiditiesValue(evt) {
    let updatedList = [...formData.comorbidities];
    if (!evt.target.checked && updatedList.includes(evt.target.value)) {
      const index = updatedList.indexOf(evt.target.value);
      if (index > -1) {
        updatedList.splice(index, 1);
      }
    } else {
      updatedList.push(evt.target.value);
    }
    setFormData((state) => ({
      ...state,
      [evt.target.name]: updatedList,
    }));
  }

  function validateUserDetails() {
    const errorsData = {};
    const nationalIDType = getNationalIdType(formData.identity);
    const nationIDNumber = getNationalIdNumber(formData.identity);

    if (!nationalIDType) {
      errorsData.nationalIDType = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_NATIONAL_ID_TYPE_ERROR_MSG
      );
    }
    if (!nationIDNumber) {
      errorsData.nationalID = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_NATIONAL_ID_ERROR_MSG
      );
    } else {
      // if(nationalIDType === ID_TYPES[0].value && isInValidAadhaarNumber(nationIDNumber)) {
      // if (nationalIDType === ID_TYPES[0].value) {
      //   errorsData.aadhaar = getMessageComponent(
      //     LANGUAGE_KEYS.BENEFICIARY_ERROR_ID_MESSAGE
      //   );
      // }
    }
    if (!formData.name) {
      errorsData.name = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_NAME_ERROR_MSG
      );
    } else if (formData.name.length < 2) {
      errorsData.name = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_MINIMUM_LENGTH_OF_NAME_ERROR_MSG
      );
    } else if (formData.name.length > 99) {
      errorsData.name = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_MAXIMUM_LENGTH_OF_NAME_ERROR_MSG
      );
    } else {
      if (!isValidName(formData.name)) {
        errorsData.name = getMessageComponent(
          LANGUAGE_KEYS.BENEFICIARY_INVALID_NAME_ERR_MSG
        );
      }
    }
    if (!formData.state) {
      errorsData.state = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_STATE_ERROR_MSG
      );
    }
    if (!formData.district) {
      errorsData.district = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_DISTRICT_ERROR_MSG
      );
    }

    if (!formData.nationalId) {
      errorsData.nationalId = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_NATIONALITY_ERROR_MSG
      );
    }

    if (formData.pincode) {
      errorsData.pincode = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_PINCODE_ERROR_MESSAGE
      );
    }

    if (!formData.gender) {
      errorsData.gender = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_GENDER_ERROR_MSG
      );
    }
    if (formData.email != "" && formData.email != formData.confirmEmail) {
      errorsData.email = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_EMAIL_ERROR_MESSAGE
      );
    }

    setErrors(errorsData);
    return Object.values(errorsData).filter((e) => e).length > 0;
    // TODO: add validations before going to confirm screen
  }

  function onContinueClick() {
    if (verifyDetails) {
      onContinue(formData);
    } else {
      if (validateUserDetails()) {
        alert("Please fill all the required field");
      } else {
        onContinue(formData);
      }
    }
  }

    return (
        <div className="text-left verify-mobile-container" ref={walkInForm}>
            <IdDetails formData={formData} setIdValue={setIdValue} setValue={setValue} setCheckBoxValue={setCheckBoxValue} verifyDetails={verifyDetails} errors={errors}/>
            <BeneficiaryDetails formData={formData} setValue={setValue} verifyDetails={verifyDetails}
                                errors={errors}/>
            <HistoryAndPhysicalExam
                formData={formData}
                setValue={setValue}
                setRadioValue={setRadioValue}
                setComorbiditiesValue={setComorbiditiesValue}
                verifyDetails={verifyDetails}
                errors={errors}
            />
            <ContactInfo formData={formData} setValue={setValue} verifyDetails={verifyDetails} errors={errors}/>
            <VaccineDetails formData={formData} setValue={setValue} verifyDetails={verifyDetails} errors={errors}/>
            <CustomButton className="primary-btn w-100 mt-5 mb-5"
                          onClick={onContinueClick}>{buttonText}</CustomButton>
        </div>
    )
}

const IdDetails = ({ verifyDetails, formData, setIdValue, setValue, setCheckBoxValue, errors }) => {
  const [motherHoodVisible, setMotherhoodVisible] = useState(false);

  function onIdChange(event, type) {
    console.log(type)
    if (type === "idType") {
      console.log(event.target.value)
      const idValue = event.target.value;
      let existingIdNumber = "";
      if (formData.identity) {
        console.log(formData.identity)
        const nationalIdNumber = getNationalIdNumber(formData.identity);
        existingIdNumber = nationalIdNumber ? nationalIdNumber : "";
      }
      let identity = constuctNationalId(idValue, existingIdNumber);
      setIdValue(identity);
    } else if (type === "idNumber") {
      const idNumber = event.target.value;
      let existingIdType = "";
      if (formData.identity) {
        const nationalIdType = getNationalIdType(formData.identity);
        existingIdType = nationalIdType ? nationalIdType : "";
      }
      let identity = constuctNationalId(existingIdType, idNumber);
      setIdValue(identity);
    }
  }

  function setGenderValue(event){
    const index = event.target.selectedIndex;
    const optionElement = event.target.childNodes[index];
    const optionElementId = optionElement.getAttribute('id');
    console.log(event)
    setValue(event);
    if(optionElementId == "app.beneficiary.gendersListOne"){
      setMotherhoodVisible(false)
    } else if(optionElementId ==  "app.beneficiary.gendersListTwo"){
      setMotherhoodVisible(true)
    }
  }

  return (
    <div>
      <h5>{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_ID_DETAILS)}</h5>
      <div>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label required"
          }
          htmlFor="name"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_NAME)}
        </label>
        <input
          className="form-control"
          name="name"
          id="name"
          type="text"
          hidden={verifyDetails}
          defaultValue={formData.name}
          onBlur={setValue}
        />
        <div className="invalid-input">{errors.name}</div>
        {verifyDetails && <p>{formData.name}</p>}
      </div>
      <div>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label required"
          }
          htmlFor="idType"
          hidden={verifyDetails}
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_ID_TYPE)}{" "}
        </label>
        <select
          className="form-control"
          id="idType"
          hidden={verifyDetails}
          onChange={(e) => onIdChange(e, "idType")}
        >
          <FormattedMessage
            id={LANGUAGE_KEYS.BENEFICIARY_SELECT_ID_TYPE}
          >
            {(message) => <option disabled selected
            >{message}</option>}
          </FormattedMessage>
          {ID_TYPES.map((id) => (
            <FormattedMessage
              id={id.name}
            >
              {(message) => <option value={id.value}
              >{message}</option>}
            </FormattedMessage>
          ))}
        </select>
        <div className="invalid-input">{errors.nationalIDType}</div>
        {/* {verifyDetails && <p>{getSelectedIdType()}</p>} */}
      </div>
      <div>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label required"
          }
          htmlFor="idNumber"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_ID_NUMBER)}{" "}
        </label>
        <input
          className="form-control"
          name="identity"
          id="idNumber"
          hidden={verifyDetails}
          type="text"
          defaultValue={getNationalIdNumber(formData.identity)}
          onBlur={(e) => onIdChange(e, "idNumber")}
        />
        <div className="invalid-input">{errors.nationalID}</div>
        <div className="invalid-input">{errors.aadhaar}</div>
        {verifyDetails && <p>{getNationalIdNumber(formData.identity)}</p>}
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.registrationDate}
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"
          }
          htmlFor="registrationDate"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_REGISTRATION_DATE)}
        </label>
        <input
          className="form-control"
          name="registrationDate"
          id="registrationDate"
          type="date"
          hidden={verifyDetails}
          defaultValue={formData.registrationDate}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.registrationDate}</p>}
      </div>
      <div>
        <label
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="name"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_AGE)}
        </label>
        <div>
          {" "}
          {new Date().getFullYear() - formData.yob}{" "}
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_YEARS)}
        </div>
      </div>
      <div>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label required"
          }
          htmlFor="gender"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_GENDER)}{" "}
        </label>
        <select
          className="form-control"
          id="gender"
          name="gender"
          onChange={(event) =>setGenderValue(event)}
          hidden={verifyDetails}
        >
          <FormattedMessage
            id={LANGUAGE_KEYS.BENEFICIARY_SELECT_GENDER}
          >
            {(message) => <option disabled selected={!formData.gender}>{message}</option>}
          </FormattedMessage>
          
          {GENDERS.map((id) => (
            <FormattedMessage
              id={"app.enrollment." + id}
            >
              {(message) => <option selected={message === formData.gender} value={id} id={id}>{message}</option>}
            </FormattedMessage>
          ))}
        </select>
        {verifyDetails && (
          <>
            <br />
            <p>{formData.gender}</p>
          </>
        )}
        <div className="invalid-input">{errors.gender}</div>
      </div>
      {motherHoodVisible ? (<div>
        <div>
          <input
              name="pregnant"
              type="checkbox"
              value="pregnant"
              id="pregnant"
              hidden={verifyDetails}
              onChange={(e) => {
              }}
              onBlur={setCheckBoxValue}
            />
            <label
              hidden={verifyDetails && !formData.pregnant}
             className={
              verifyDetails
                ? "custom-verify-text-label"
                : "custom-text-label"
            } htmlFor="pregnant">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_PREGNANT)}
          </label>
        </div>
        <div>
          <input
              name="lactating"
              type="checkbox"
              value="lactating"
              id="lactating"
              hidden={verifyDetails}
              onChange={(e) => {
              }}
              onBlur={setCheckBoxValue}
            />
            <label hidden={verifyDetails && !formData.lactating}
             className={
              verifyDetails
                ? "custom-verify-text-label"
                : "custom-text-label"} htmlFor="lactating">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_LACTATING)}
          </label>
        </div>
      </div>): null}
      <br/>
      <div>
        <label
          hidden={verifyDetails && !formData.relativeName}
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"
          }
          htmlFor="relativeName"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RELATIVE_NAME)}
        </label>
        <input
          className="form-control"
          name="relativeName"
          id="relativeName"
          type="text"
          hidden={verifyDetails}
          defaultValue={formData.relativeName}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.relativeName}</p>}
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.relativePhoneNumber}
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"
          }
          htmlFor="relativePhoneNumber"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RELATIVE_PHONE_NUMBER)}
        </label>
        <input
          className="form-control"
          name="relativePhoneNumber"
          id="relativePhoneNumber"
          type="tel"
          hidden={verifyDetails}
          defaultValue={formData.relativePhoneNumber}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.relativePhoneNumber}</p>}
      </div>
    </div>
  );
};

const BeneficiaryDetails = ({ verifyDetails, formData, setValue, errors }) => {
  const state_and_districts = useSelector(
    (state) => state.flagr.appConfig.stateAndDistricts
  );
  const STATES = Object.values(state_and_districts["states"]).map(
    (obj) => obj.name
  );

  const [districts, setDistricts] = useState([]);
  const [nationalities, setNationalities] = useState([]);
  // Change the comorbidities and occupation to an api call
  const [comorbidities, setComorbidities] = useState([COMORBIDITIES]);
  const [occupation, setOccupation] = useState([OCCUPATIONS]);

  useEffect(() => {
    setDistictsForState(formData.state);
    applicationConfigsDB.getApplicationConfigs().then((res) => {
      setNationalities(res.nationalities);
    });
  }, []);

  function onStateSelected(stateSelected) {
    setValue({ target: { name: "state", value: stateSelected } });
    setValue({ target: { name: "district", value: "" } });
    setDistictsForState(stateSelected);
  }

  function setDistictsForState(state) {
    const stateObj = Object.values(state_and_districts["states"]).find(
      (obj) => obj.name === state
    );
    if (stateObj) {
      setDistricts(stateObj.districts);
    } else {
      setDistricts([]);
    }
  }

  function setDobValue(dob) {
    setValue({ target: { name: "dob", value: dob } });
  }

  return (
    <div className="pt-3">
      <h5>
        {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RESIDENCE_DETAILS)}
      </h5>
      <div>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label required"
          }
          htmlFor="state"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_REGION)}{" "}
        </label>
        <select
          className="form-control"
          name="state"
          id="state"
          onChange={(e) => onStateSelected(e.target.value)}
          hidden={verifyDetails}
        >
          <FormattedMessage
            id={LANGUAGE_KEYS.BENEFICIARY_SELECT_REGION}
          >
            {(message) => <option disabled selected={!formData.state}>{message}</option>}
          </FormattedMessage>
          {STATES.map((id) => (
            <option selected={id === formData.state} value={id}>
              {id}
            </option>
          ))}
        </select>
        <div className="invalid-input">{errors.state}</div>
        {verifyDetails && <p>{formData.state}</p>}
      </div>
      <div>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label required"
          }
          htmlFor="district"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_SUBCITY_ZONE)}{" "}
        </label>
        <select
          className="form-control"
          id="district"
          name="district"
          onChange={setValue}
          hidden={verifyDetails}
        >
          <FormattedMessage
            id={LANGUAGE_KEYS.BENEFICIARY_SELECT_SUBCITY_ZONE}
          >
            {(message) => <option disabled selected={!formData.district}>{message}</option>}
          </FormattedMessage>
          {districts.map((d) => (
            <option selected={d.name === formData.district} value={d.name}>
              {d.name}
            </option>
          ))}
        </select>
        <div className="invalid-input">{errors.district}</div>
        {verifyDetails && <p>{formData.district}</p>}
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.locality}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="locality"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_WOREDA)}
        </label>
        <input
          className="form-control"
          name="locality"
          id="locality"
          type="text"
          hidden={verifyDetails}
          defaultValue={formData.locality}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.locality}</p>}
      </div>
      <div>
        <label
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="occupation"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_OCCUPATION)}
        </label>
        <select
          className="form-control"
          name="occupation"
          id="occupation"
          onChange={setValue}
          hidden={verifyDetails}
        >
          <FormattedMessage
            id={LANGUAGE_KEYS.BENEFICIARY_SELECT_OCCUPATION}
          >
            {(message) => <option disabled selected={!formData.occupation}>{message}</option>}
          </FormattedMessage>
          {OCCUPATIONS.map((id) => (
            <FormattedMessage
              id={"app.enrollment." + id}
            >
              {(message) => <option selected={id === formData.occupation} value={id}>{message}</option>}
            </FormattedMessage>
          ))}
        </select>
        <div className="invalid-input">{errors.occupation}</div>
        {verifyDetails && <p>{formData.occupation}</p>}
      </div>
      <div>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label required"
          }
          htmlFor="state"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_NATIONALITY)}{" "}
        </label>
        <select
          className="form-control"
          name="nationalId"
          id="nationalId"
          onChange={setValue}
          hidden={verifyDetails}
        >
          <FormattedMessage
            id={LANGUAGE_KEYS.BENEFICIARY_SELECT_NATIONALITY}
          >
            {(message) => <option disabled selected={!formData.nationalId}>{message}</option>}
          </FormattedMessage>
          {NATIONALITY.map((id) => (
            <FormattedMessage
              id={"app.enrollment." + id}
            >
              {(message) => <option selected={id === formData.nationalId} value={id}>{message}</option>}
            </FormattedMessage>
          ))}
        </select>
        <div className="invalid-input">{errors.nationalId}</div>
        {verifyDetails && <p>{formData.nationalId}</p>}
      </div>
    </div>
  );
};

const HistoryAndPhysicalExam = ({
  verifyDetails,
  formData,
  setValue,
  setRadioValue,
  setComorbiditiesValue,
  errors,
}) => {

  const [onMedicationSpecifyVisible, setOnMedicationSpecifyVisible] = useState(false);
  const [alergySpecifyVisible, setAlergySpecifyVisible] = useState(false);
  const [vaccinereceivedSpecifyVisible, setVaccinereceivedSpecifyVisible] = useState(false);
  const [substanceAbuseSpecifyVisible, setSubstanceAbuseSpecifyVisible] = useState(false);


  function setRadioOnChange(evt ) {
    if(evt.target.id == "onMedication" && evt.target.checked){
      setOnMedicationSpecifyVisible(true);
    }else if(evt.target.id == "notOnMedication" && evt.target.checked){
      setOnMedicationSpecifyVisible(false)
    }if(evt.target.id == "alergies" && evt.target.checked){
      setAlergySpecifyVisible(true);
    }else if(evt.target.id == "noAlergies" && evt.target.checked){
      setAlergySpecifyVisible(false)
    }if(evt.target.id == "vaccinesReceived" && evt.target.checked){
      setVaccinereceivedSpecifyVisible(true);
    }else if(evt.target.id == "vaccinesNotReceived" && evt.target.checked){
      setVaccinereceivedSpecifyVisible(false)
    }if(evt.target.id == "abusesSubstance" && evt.target.checked){
      setSubstanceAbuseSpecifyVisible(true);
    }else if(evt.target.id == "noSubstanceAbuse" && evt.target.checked){
      setSubstanceAbuseSpecifyVisible(false)
    }
  }


  return (
    <div className="pt-3">
      <h5>{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_MEDICAL_INFO)}</h5>
      <div>
        <label
          hidden={verifyDetails && !formData.height}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="height"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_HEIGHT)}
        </label>
        <input
          className="form-control"
          name="height"
          id="height"
          type="number"
          defaultValue={formData.height}
          hidden={verifyDetails}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.height}</p>}
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.weight}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="weight"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_WEIGHT)}
        </label>
        <input
          className="form-control"
          name="weight"
          id="weight"
          type="number"
          defaultValue={formData.weight}
          hidden={verifyDetails}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.weight}</p>}
      </div>
      <div>
        <div>
          <label
            className={
              verifyDetails ? "custom-verify-text-label" : "custom-text-label"
            }
            htmlFor="onMedication"
            hidden={verifyDetails}
          >
            <b>{" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_ON_MEDICATION)}</b>
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
            name="onMedication"
            type="radio"
            value="yes"
            id="onMedication"
            onChange={(e) => {
              setRadioOnChange(e);
            }}
            onBlur={setRadioValue}
          />
          <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="onMedication">
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RADIO_YES)}
        </label>
      </div>
      <div
      hidden={verifyDetails}>
        <input
            name="onMedication"
            type="radio"
            value="no"
            id="notOnMedication"
            onChange={(e) => {
              setRadioOnChange(e);
            }}
            hidden={verifyDetails}
            onBlur={setRadioValue}
          />
          <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="notOnMedication">
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RADIO_NO)}
        </label>
      </div>
      </div>
      {onMedicationSpecifyVisible ? (<div
      hidden={verifyDetails}>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"
          }
          htmlFor="specifyOnMedication"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_SPECIFY)}
        </label>
        <input
          className="form-control"
          id="specifyOnMedication"
          name="specifyOnMedication"
          hidden={verifyDetails}
          type="text"
          onBlur={setValue}
        />
      </div>): null}
      <div>
        <div>
          <label
            hidden={verifyDetails}
            className={
              verifyDetails ? "custom-verify-text-label" : "custom-text-label"
            }
            htmlFor="isKnownAlergies"
          >
            <b>{" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_ALERGY)}</b>
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
            name="knownAllergies"
            type="radio"
            value="yes"
            id="alergies"
            onChange={(e) => {
              setRadioOnChange(e);
            }}
            onBlur={setRadioValue}
          />
          <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="alergies">
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RADIO_YES)}
        </label>
      </div>
      <div
      hidden={verifyDetails}>
        <input
            name="knownAllergies"
            type="radio"
            value="no"
            id="noAlergies"
            onChange={(e) => {
              setRadioOnChange(e);
            }}
            onBlur={setRadioValue}
          />
          <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="noAlergies">
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RADIO_NO)}
        </label>
      </div>
      </div>
      {alergySpecifyVisible ? (<div
      hidden={verifyDetails}>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"
          }
          htmlFor="specifyKnownAlergies"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_SPECIFY)}
        </label>
        <input
          className="form-control"
          id="specifyKnownAlergies"
          name="specifyKnownAlergies"
          hidden={verifyDetails}
          type="text"
          onBlur={setValue}
        />
      </div>): null}
      <div>
        <div>
          <label
            hidden={verifyDetails}
            className={
              verifyDetails ? "custom-verify-text-label" : "custom-text-label"
            }
            htmlFor="isVaccinesReceived"
          >
            <b>{" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_VACCINES_FOR_SIX_MONTHS)}</b>
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
            name="vaccinesReceived"
            type="radio"
            value="yes"
            id="vaccinesReceived"
            onChange={(e) => {
              setRadioOnChange(e);
            }}
            onBlur={setRadioValue}
          />
          <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="vaccinesReceived">
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RADIO_YES)}
        </label>
      </div>
      <div
      hidden={verifyDetails}>
        <input
            name="vaccinesReceived"
            type="radio"
            value="no"
            id="vaccinesNotReceived"
            onChange={(e) => {
              setRadioOnChange(e);
            }}
            onBlur={setRadioValue}
          />
          <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="vaccinesNotReceived">
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RADIO_NO)}
        </label>
      </div>
      </div>
      {vaccinereceivedSpecifyVisible ? (<div
      hidden={verifyDetails}>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"
          }
          htmlFor="specifyVaccinesReceived"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_SPECIFY)}
        </label>
        <input
          className="form-control"
          id="specifyVaccinesReceived"
          name="specifyVaccinesReceived"
          hidden={verifyDetails}
          type="text"
          onBlur={setValue}
        />
      </div>): null}
      <div>
        <div>
          <label
            hidden={verifyDetails}
            className={
              verifyDetails ? "custom-verify-text-label" : "custom-text-label"
            }
            htmlFor="isSubstanceAbuse"
          >
            <b>{" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_SUBSTANCE_ABUSE)}</b>
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
            name="substanceAbuse"
            type="radio"
            value="yes"
            id="abusesSubstance"
            onChange={(e) => {
              setRadioOnChange(e);
            }}
            onBlur={setRadioValue}
          />
          <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="abusesSubstance">
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RADIO_YES)}
        </label>
      </div>
      <div
      hidden={verifyDetails}>
        <input
            name="substanceAbuse"
            type="radio"
            value="no"
            id="noSubstanceAbuse"
            onChange={(e) => {
              setRadioOnChange(e);
            }}
            onBlur={setRadioValue}
          />
          <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="noSubstanceAbuse">
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RADIO_NO)}
        </label>
        </div>
      </div>
      {substanceAbuseSpecifyVisible ? (<div
      hidden={verifyDetails}>
        <label
          className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"
          }
          htmlFor="specifySubstanceAbuse"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_SPECIFY)}
        </label>
        <input
          className="form-control"
          id="specifySubstanceAbuse"
          name="specifySubstanceAbuse"
          hidden={verifyDetails}
          type="text"
          onBlur={setValue}
        />
      </div>): null}
      
      <div>
        <div>
          <label
            hidden={verifyDetails && (formData.comorbidities.length == 0)}
            className={
              verifyDetails ? "custom-verify-text-label" : "custom-text-label"
            }
            htmlFor="comorbidity"
          >
            <b>{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_COMORBID_CONDITIONS)}</b>
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
              name="comorbidities"
              type="checkbox"
              value="hypertension"
              id="hypertension"
              onChange={(e) => {
              }}
              onBlur={setComorbiditiesValue}
            />
            <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="hypertension">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_COMO_HYPER)}
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
              name="comorbidities"
              type="checkbox"
              value="asthma"
              id="asthma"
              onChange={(e) => {
              }}
              onBlur={setComorbiditiesValue}
            />
            <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="asthma">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_COMO_ASTHMA)}
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
              name="comorbidities"
              type="checkbox"
              value="hiv"
              id="hiv"
              onChange={(e) => {
              }}
              onBlur={setComorbiditiesValue}
            />
            <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="hiv">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_COMO_HIV)}
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
              name="comorbidities"
              type="checkbox"
              value="chronicKidneyDisease"
              id="chronicKidneyDisease"
              onChange={(e) => {
              }}
              onBlur={setComorbiditiesValue}
            />
            <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="chronicKidneyDisease">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_COMO_KIDNEY)}
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
              name="comorbidities"
              type="checkbox"
              value="diabetes"
              id="diabetes"
              onChange={(e) => {
              }}
              onBlur={setComorbiditiesValue}
            />
            <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="diabetes">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_COMO_DIABETES)}
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
              name="comorbidities"
              type="checkbox"
              value="cardiacProblem"
              id="cardiacProblem"
              onChange={(e) => {
              }}
              onBlur={setComorbiditiesValue}
            />
            <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="lactating">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_COMO_CARDIAC)}
          </label>
        </div>
        <div
        hidden={verifyDetails}>
          <input
              name="comorbidities"
              type="checkbox"
              value="epilepsy"
              id="epilepsy"
              
              onChange={(e) => {
              }}
              onBlur={setComorbiditiesValue}
            />
            <label className={
            verifyDetails
              ? "custom-verify-text-label"
              : "custom-text-label"} htmlFor="epilepsy">
            {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_COMO_EPILEPSY)}
          </label>
        </div>
        {formData.comorbidities.map((comorbid) => (
          <div>
          {verifyDetails && <p>{comorbid}</p>}
          </div>
        ))}
      </div>
      <h5>{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_VITAL_SIGNS)}</h5>
      <div>
        <label
          hidden={verifyDetails && !formData.pulseRate}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="pulseRate"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_PULSE_RATE)}
        </label>
        <input
          className="form-control"
          name="pulseRate"
          id="pulseRate"
          type="number"
          defaultValue={formData.pulseRate}
          hidden={verifyDetails}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.pulseRate}</p>}
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.respiratoryRate}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="respiratoryRate"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_RESPIRATORY_RATE)}
        </label>
        <input
          className="form-control"
          name="respiratoryRate"
          id="respiratoryRate"
          type="number"
          defaultValue={formData.respiratoryRate}
          hidden={verifyDetails}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.respiratoryRate}</p>}
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.systolicBloodPressure && !formData.diastolicBloodPressure}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="bloodPressure"
        >
          <b>{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_BLOOD_PRESSURE)}</b>
        </label>

        <div>
          <label
            hidden={verifyDetails && !formData.systolicBloodPressure}
            className={
              verifyDetails ? "custom-verify-text-label" : "custom-text-label"
            }
            htmlFor="systolicBloodPressure"
          >
            {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_BLOOD_PRESSURE_SYSTOLIC)}
          </label>
          <input
            className="form-control"
            name="systolicBloodPressure"
            id="systolicBloodPressure"
            type="text"
            defaultValue={formData.systolicBloodPressure}
            hidden={verifyDetails}
            onBlur={setValue}
          />
          {verifyDetails && <p>{formData.systolicBloodPressure}</p>}
        </div>
        <div>
          <label
            hidden={verifyDetails && !formData.systolicBloodPressure}
            className={
              verifyDetails ? "custom-verify-text-label" : "custom-text-label"
            }
            htmlFor="diastolicBloodPressure"
          >
            {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_BLOOD_PRESSURE_DIASTOLIC)}
          </label>
          <input
            className="form-control"
            name="diastolicBloodPressure"
            id="diastolicBloodPressure"
            type="text"
            defaultValue={formData.diastolicBloodPressure}
            hidden={verifyDetails}
            onBlur={setValue}
          />
          {verifyDetails && <p>{formData.diastolicBloodPressure}</p>}
        </div>
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.temperature}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="temperature"
        >
          {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_TEMPERATURE)}
        </label>
        <input
          className="form-control"
          name="temperature"
          id="temperature"
          type="number"
          defaultValue={formData.temperature}
          hidden={verifyDetails}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.temperature}</p>}
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.consciousnessLevel}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="consciousnessLevel"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_CONSCIOUSNESS_LEVEL)}
        </label>
        <input
          className="form-control"
          name="consciousnessLevel"
          id="consciousnessLevel"
          type="text"
          defaultValue={formData.consciousnessLevel}
          hidden={verifyDetails}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.consciousnessLevel}</p>}
      </div>
      <div>
        <label
          hidden={verifyDetails && !formData.spoTwo}
          className={
            verifyDetails ? "custom-verify-text-label" : "custom-text-label"
          }
          htmlFor="spoTwo"
        >
          {" "}{getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_SPO_TWO)}
        </label>
        <input
          className="form-control"
          name="spoTwo"
          id="spoTwo"
          type="number"
          defaultValue={formData.spoTwo}
          hidden={verifyDetails}
          onBlur={setValue}
        />
        {verifyDetails && <p>{formData.spoTwo}</p>}
      </div>
    </div>
  );
};
const ContactInfo = ({ verifyDetails, formData, setValue, errors }) => {
    const userMobileNumber = "";

    return (
        <div className="pt-3">
            <h5>
                {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_CONTACT_INFORMATION)}
            </h5>
            <div>
                <label
                    className={
                        verifyDetails ? "custom-verify-text-label" : "custom-text-label"
                    }
                    htmlFor="mobile"
                >
                    {getMessageComponent(LANGUAGE_KEYS.BENEFICIARY_MOBILE)}
                </label>
                {!verifyDetails && (
                    <div className="radio-group">
                        <div className="pb-2">
                            <label className="form-check-label" htmlFor="defaultContact">
                                {formData.phone}
                            </label>
                        </div>
                    </div>
                )}
                {verifyDetails && (
                    <>
                        <br/>
                        <p>{formData.phone}</p>
                    </>
                )}
            </div>
        </div>
    )
};

const VaccineDetails = ({verifyDetails, formData, setValue, errors}) => {

    const hasVaccine = formData.appointments ?
        formData.appointments.filter(a => a["programId"] === getSelectedProgramId() && a.vaccine).length > 0 : false;

    return (
        <div className="pt-3">
          <div hidden={!hasVaccine}>
              <h5>Vaccine Details</h5>
              <div>
                  <label className={verifyDetails ? "custom-verify-text-label" : "custom-text-label"}
                          htmlFor="mobile">Vaccine</label>
                  {
                      verifyDetails &&
                      <><br/>
                          <p>
                              {formData.appointments.filter(a => a["programId"] === getSelectedProgramId() && !a.certified)[0]?.vaccine}
                              {
                                  <DosesState appointments={formData.appointments}/>
                              }
                          </p></>
                  }
              </div>
          </div>
        </div>
    )
};

