import React, { useState } from "react";
import { BaseFormCard } from "../BaseFormCard";
import { getMessageComponent, LANGUAGE_KEYS } from "../../lang/LocaleContext";
import "./index.css";
import { CustomButton } from "../CustomButton";
import {
  FORM_WALK_IN_ENROLL_FORM,
  useWalkInEnrollment,
  initialWalkInEnrollmentState,
} from "../WalkEnrollments/context";
import {
  FORM_WALK_IN_VERIFY_MOBILE,
  FORM_WALK_IN_VERIFY_OTP,
} from "../WalkEnrollments/context";
import { useOnlineStatus } from "../../utils/offlineStatus";

export const VerifyMobile = ({state}) => {
  const isOnline = useOnlineStatus();
  const [phone, setPhone] = useState("");
  const [errors, setErrors] = useState({});
  const { goNext } = useWalkInEnrollment();
  const [formData, setFormData] = useState({ ...state });

  function onGetOTP() {
    if (phone.length === 10) {
      goNext(FORM_WALK_IN_VERIFY_MOBILE, FORM_WALK_IN_VERIFY_OTP, { phone });
    } else {
      setErrors({ mobile: "Invalid mobile number" });
    }
  }

  function setValue(data) {
    setFormData((state) => ({ ...state, ...data }));
  }

  function onPhoneChange(phone) {
    setValue({ phone: phone });
  }

  function goToForm() {
    // setPhoneNumber(phone);
    console.log(formData)
    goNext(FORM_WALK_IN_VERIFY_OTP, FORM_WALK_IN_ENROLL_FORM, formData);
  }

  function onContinueClick() {
    if (validateUserDetails()) {
      alert("Please fill all the required field");
    } else {
      goToForm();
    }
  }

  function validateUserDetails() {
    const errorsData = {};

    if (!formData.phone) {
      errorsData.mobile = getMessageComponent(
        LANGUAGE_KEYS.BENEFICIARY_MOBILE_ERROR_MSG
      );
    }

    setErrors(errorsData);
    return Object.values(errorsData).filter((e) => e).length > 0;
    // TODO: add validations before going to confirm screen
  }

  return (
    <div className="new-enroll-container">
      <BaseFormCard title={getMessageComponent(LANGUAGE_KEYS.ENROLLMENT_TITLE)}>
        <div className="verify-mobile-container">
          <h5>
            {getMessageComponent(
              isOnline
                ? LANGUAGE_KEYS.VERIFY_MOBILE
                : LANGUAGE_KEYS.VERIFY_ENTER_MOBILE
            )}
          </h5>
          <input
            className="w-100 mt-5 mb-3 mobile-input"
            type="tel"
            value={formData.phone}
            onChange={(t) => onPhoneChange(t.target && t.target.value)}
            autoFocus={true}
            //    placeholder="Enter mobile number"
            maxLength={10}
          />
          <div className="invalid-input m-0 text-left">{errors.mobile}</div>
          {isOnline ? (
            <CustomButton className="primary-btn w-100" onClick={onContinueClick}>
              {getMessageComponent(LANGUAGE_KEYS.VERIFY_CONTINUE)}
            </CustomButton>
          ) : (
            <CustomButton className="primary-btn w-100" onClick={onContinueClick}>
              {getMessageComponent(LANGUAGE_KEYS.VERIFY_CONTINUE)}
            </CustomButton>
          )}
        </div>
      </BaseFormCard>
    </div>
  );
};
