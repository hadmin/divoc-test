import React, {createContext, useContext, useMemo, useReducer} from "react";
import {useHistory} from "react-router";
import { CONSTANT } from "utils/constants";
import {appIndexDb} from "../../AppDatabase";
import config from "../../config"
import {getSelectedProgram} from "../ProgramSelection";

export const FORM_WALK_IN_ELIGIBILITY_CRITERIA = "eligibility_criteria";
export const FORM_WALK_IN_VERIFY_MOBILE = "verify_mobile";
export const FORM_WALK_IN_VERIFY_OTP = "verify_otp";
export const FORM_WALK_IN_ENROLL_FORM = "form";
export const FORM_WALK_IN_VERIFY_FORM = "verify_form";
export const FORM_WALK_IN_ENROLL_PAYMENTS = "payments";
export const FORM_WALK_IN_ENROLL_CONFIRMATION = "confirm";
export const INVALID_ELIGIBILITY_CRITERIA = "invalid_eligibility_criteria"

export const WALK_IN_ROUTE = "walkInEnroll";

const WalkInEnrollmentContext = createContext(null);

const initialWalkInEnrollmentStateCovax = {
    comorbidities: [],
    yob: "",
    choice: "yes",
    currentForm: FORM_WALK_IN_ELIGIBILITY_CRITERIA,
    nextForm: FORM_WALK_IN_ELIGIBILITY_CRITERIA,

    programId: "",
    programName: "",
    nationalId: "",
    identity: "",
    name: "",
    gender: "",
    district: "",
    state: "",
    contact: "",
    email: "",
    confirmEmail: "",
    status: null,
    locality: "",
    comorbidity: "",
    occupation: "",
    registrationDate: new Date().toISOString().split('T')[0],
    pulseRate: "",
    respiratoryRate: "",
    systolicBloodPressure:"",
    diastolicBloodPressure: "",
    temperature: "",
    pincode: "",
    phone: "",
    beneficiaryPhone: "",
    appointments: [],
    relativeName: "",
    relativePhonenumber:"",
    pregnant:false,
    lactating:false,
    height:"",
    weight:"",
    onMedication: false,
    specifyOnMedication:"",
    knownAllergies:false,
    specifyKnownAlergies:"",
    vaccinesReceived:false,
    specifyVaccinesReceived:"",
    substanceAbuse: false,
    specifySubstanceAbuse: "",
    consciousnessLevel:"",
    spoTwo:""
};

const initialWalkInEnrollmentStateInfant = {
    comorbidities: [],
    yob: "",
    choice: "yes",
    currentForm: FORM_WALK_IN_ELIGIBILITY_CRITERIA,
    nextForm: FORM_WALK_IN_ELIGIBILITY_CRITERIA,

    programId: "",
    programName: "",
    nationalId: "",
    identity: "",
    name: "",
    gender: "",
    district: "",
    state: "",
    contact: "",
    email: "",
    confirmEmail: "",
    status: null,
    locality: "",
    comorbidity: "",
    phone: "",
    appointments: [],
    serialNumber: "",
    programName: "",
    mrn: "",
    motherName: "",
    motherMrn: "",
    motherTTlastPregnancy: null,
    motherTTtotal: null,
    registrationDate: new Date(),
    state: "",
    district: "",
    wereda: "",
    ketena: "",
};

// export const initialWalkInEnrollmentState = (getSelectedProgram() == CONSTANT.INFANT_ROUTINE ? initialWalkInEnrollmentStateInfant : initialWalkInEnrollmentStateCovax);
export const initialWalkInEnrollmentState = initialWalkInEnrollmentStateCovax;

export function WalkInEnrollmentProvider(props) {
    const [state, dispatch] = useReducer(walkInEnrollmentReducer, initialWalkInEnrollmentState);
    const value = useMemo(() => [state, dispatch], [state]);
    return <WalkInEnrollmentContext.Provider value={value} {...props} />
}


function walkInEnrollmentReducer(state, action) {
    switch (action.type) {
        case FORM_WALK_IN_ELIGIBILITY_CRITERIA:
        case FORM_WALK_IN_VERIFY_MOBILE:
        case FORM_WALK_IN_VERIFY_OTP:
        case FORM_WALK_IN_VERIFY_FORM:
        case FORM_WALK_IN_ENROLL_CONFIRMATION:
        case INVALID_ELIGIBILITY_CRITERIA:
        case FORM_WALK_IN_ENROLL_PAYMENTS: {
            return {
                ...state,
                ...action.payload
            };
        }
        case FORM_WALK_IN_ENROLL_FORM: {
            const newState = {
                ...state,
                ...action.payload
            };
            newState.address = {
                addressLine1: "",
                addressLine2: "",
                district: action.payload.district,
                state: action.payload.state,
                pincode: action.payload.pincode
            };
            return newState
        }
        default:
            return  state;
    }
}

export function useWalkInEnrollment() {

    const context = useContext(WalkInEnrollmentContext);
    const history = useHistory();
    if (!context) {
        throw new Error(`useWalkInEnrollment must be used within a WalkInEnrollmentProvider`)
    }
    const [state, dispatch] = context;

    const goNext = function (current, next, payload) {
        console.log(current, next)
        payload.currentForm = current;
        payload.nextForm = next;
        dispatch({type: current, payload: payload});
        if (next) {
            if (next === '/') {
                history.replace(config.urlPath, null)
            } else {
                history.push(config.urlPath + '/' + WALK_IN_ROUTE + '/' + next)
            }
        }
    };

    const goBack = function () {
        history.goBack()
    };

    const saveWalkInEnrollment = async function (formData) {
            // paymentMode
        // state.paymentMode = paymentMode ?? "NA";
        return appIndexDb.saveWalkInEnrollments(state)
    };

    const saveWalkInEnrollmentInfant = async function (formData) {
            // paymentMode
        // state.paymentMode = paymentMode ?? "NA";
        return appIndexDb.saveWalkInEnrollmentsInfant(state)
    };

    return {
        state,
        dispatch,
        goNext,
        goBack,
        saveWalkInEnrollment,
        saveWalkInEnrollmentInfant
    }
}
