import React, { useContext, useState } from "react";
import {
  FormattedMessage,
  FormattedNumber,
  IntlProvider,
  useIntl,
} from "react-intl";
import Hindi from "../lang/hi.json";
import English from "../lang/en.json";
import Arabic from "../lang/ar.json";
import Amharic from "../lang/amh.json";
import Tigrinya from "../lang/tg.json";
import Oromiffa from "../lang/oro.json";
import Somali from "../lang/som.json";

const LocaleContext = React.createContext(null);

const local = localStorage.getItem("language") || navigator.language;

let lang;
if (local === "hi") {
  lang = Hindi;
} else if (local === "ar") {
  lang = Arabic;
} else if (local === "amh") {
  lang = Amharic;
} else if (local === "tg") {
  lang = Tigrinya;
} else if (local === "oro") {
  lang = Oromiffa;
} else if (local === "som") {
  lang = Somali;
} else {
  lang = English;
}

export function LocaleProvider(props) {
  const [locale, setLocale] = useState(local);
  const [messages, setMessages] = useState(lang);

  function selectLanguage(newLocale) {
    setLocale(newLocale);

    if (newLocale === "hi") {
      setMessages(Hindi);
    } else if (newLocale === "ar") {
      setMessages(Arabic);
    } else if (newLocale === "amh") {
      setMessages(Amharic);
    } else if (newLocale === "tg") {
      setMessages(Tigrinya);
    } else if (newLocale === "oro") {
      setMessages(Oromiffa);
    } else if (newLocale === "som") {
      setMessages(Somali);
    } else {
      setMessages(English);
    }
    localStorage.setItem("language", newLocale);
  }

  return (
    <LocaleContext.Provider value={{ locale, selectLanguage }}>
      <IntlProvider messages={messages} locale={locale}>
        {props.children}
      </IntlProvider>
    </LocaleContext.Provider>
  );
}

export function useLocale() {
  const context = useContext(LocaleContext);
  const intl = useIntl();
  const { selectLanguage } = context;
  const currentLocale = () => {
    return localStorage.getItem("language") || navigator.language;
  };

  const getText = (key) => intl.formatMessage({ id: key });
  return {
    selectLanguage,
    currentLocale,
    getText,
  };
}

export function getMessageComponent(id, defaultMessage, values) {
  return (
    <FormattedMessage
      id={id}
      values={values || {}}
      defaultMessage={defaultMessage || ""}
    />
  );
}

export function getNumberComponent(number) {
  return <FormattedNumber value={number} />;
}

export const LANGUAGE_KEYS = Object.freeze({
  HOME: "app.home",
  QUEUE: "app.queue",
  LANGUAGE: "app.language",
  HELP: "app.help",
  PROGRAM: "app.program",
  LOGOUT: "app.logout",
  PROFILE: "app.profile",
  ACTIONS: "app.actionTitle",
  STATUS_OPEN: "app.statusOpen",
  STATUS_COMPLETED: "app.statusCompleted",
  STATUS_BOOKED: "app.statusBooked",
  RECIPIENT_NUMBERS: "app.recipientDetailsTitle",
  ENROLLMENT_TODAY: "app.enrollmentToday",
  APPOINTMENT_TODAY: "app.appointmentToday",
  MORNING_SCHEDULE: "app.morningSchedule",
  AFTERNOON_SCHEDULE: "app.afternoonSchedule",
  STATUS_ONGOING: "app.statusOngoing",
  SELECT_LANGUAGE: "app.selectLanguage",
  SELECT_PROGRAM: "app.selectProgram",
  VERIFY_RECIPIENT: "app.verifyRecipient",
  ENROLL_RECIPIENT: "app.enrollRecipient",
  RECIPIENT_QUEUE: "app.recipientQueue",
  RECIPIENT_QUEUE_WITH_SIZE: "app.recipientQueueWithSize",
  RECIPIENT_WAITING: "app.recipientWaiting",
  CERTIFICATE_ISSUED: "app.certificateIssued",
  ENTER_IDENTITY_NUMBER: "app.enterIdentityNumber",
  REGISTER_IDENTITY_NUMBER: "app.registerIdentityNumber",
  SCAN_IDENTITY_NUMBER: "app.scanIdentityNumber",
  LIMIT_REACH_MESSAGE: "app.limitReachMessage",
  RECIPIENTS_ENROLLED: "app.recipientsEnrolled",
  EXCEED_LIMITS: "app.exceedLimits",
  NAME: "app.name",

  RECIPIENT_ENTER_ENROLMENT_NUMBER:
    "app.verifyRecipient.enterMobileAndVerificationCode",
  VERIFY_RECIPIENT_CONFIRM_BUTTON: "app.verifyRecipient.confirmButton",
  RECIPIENT_QUEUE_MESSAGE: "app.recipientQueue.message",
  RECIPIENT_QUEUE_STATUS: "app.recipientQueue.status",
  RECIPIENT_QUEUE_NUMBER: "app.recipientQueue.number",
  RECIPIENT_QUEUE_TITLE: "app.recipientQueue.title",

  PROFILE_LAST_LOGGED_IN: "app.profile.lastLoggedIn",
  PROFILE_LAST_SYNC: "app.profile.lastSync",
  PROFILE_FACILITY: "app.profile.facility",
  PROFILE_CONFIRM_LOGOUT_MESSAGE: "app.profile.confirmLogout.message",
  PROFILE_CONFIRM_LOGOUT_OK: "app.profile.confirmLogout.ok",
  PROFILE_CONFIRM_LOGOUT_CANCEL: "app.profile.confirmLogout.cancel",

  PRE_ENROLLMENT_TITLE: "app.preenrollment.title",
  PRE_ENROLLMENT_DETAILS: "app.preenrollment.details",
  PRE_ENROLLMENT_ENTER_MANUALLY: "app.preenrollment.enterManually",
  PRE_ENROLLMENT_CONTINUE: "app.preenrollment.continue",
  PRE_ENROLLMENT_NO_PATIENTS: "app.preenrollment.noPatient",
  PRE_ENROLLMENT_ENTER_OTP: "app.preenrollment.enterOTP",
  RECIPIENT_SCAN_QR_CODE: "app.preenrollment.scanQrCode",

  ENROLLMENT_TITLE: "app.enrollment.title",
  ENROLLMENT_NAME: "app.enrollment.name",
  ENROLLMENT_SELECT_GENDER: "app.enrollment.gender",
  ENROLLMENT_MALE: "app.enrollment.male",
  ENROLLMENT_FEMALE: "app.enrollment.female",
  ENROLLMENT_NATIONALITY: "app.enrollment.nationalId",
  ENROLLMENT_ETHIOPIAN: "app.enrollment.ethiopian",
  ENROLLMENT_NATIONALITYOTHER: "app.enrollment.nationalityOther",
  ENROLLMENT_HEALTH_PROFESSIONAL: "app.enrollment.health",
  ENROLLMENT_NON_HEALTH_PROFESSIONAL: "app.enrollment.nonHealth",
  ENROLLMENT_DOB: "app.enrollment.dob",
  ENROLLMENT_EMAIL: "app.enrollment.email",
  ENROLLMENT_MOBILE: "app.enrollment.phone",
  ENROLLMENT_IDENTITY_TYPE: "app.enrollment.identityType",
  ENROLLMENT_IDENTITY_NO: "app.enrollment.identityValue",
  ENROLLMENT_ADDRESS_1: "app.enrollment.addressLine1",
  ENROLLMENT_STATE: "app.enrollment.state",
  ENROLLMENT_DISTRICT: "app.enrollment.district",
  ENROLLMENT_LOCALITY: "app.enrollment.locality",
  ENROLLMENT_PINCODE: "app.enrollment.pincode",

  PAYMENT_TITLE: "app.payment.title",
  PAYMENT_GOVT: "app.payment.govt",
  PAYMENT_VOUCHER: "app.payment.voucher",
  PAYMENT_DIRECT: "app.payment.direct",

  BUTTON_DONE: "app.button.done",
  BUTTON_NEXT: "app.button.next",
  BUTTON_CONFIRM: "app.button.confirm",
  BUTTON_CONTINUE: "app.button.continue",
  BUTTON_SEND_FOR_VACCINATION: "app.button.sendvaccination",
  BUTTON_OTP: "app.button.otp",
  BUTTON_VERIFY: "app.button.verify",

  VACCINATION_TITLE: "app.vaccination.title",
  VACCINATION_VACCINATOR: "app.vaccination.vaccinator",
  VACCINATION_VACCINE: "app.vaccination.vaccine",
  VACCINATION_BATCH_ID: "app.vaccination.batchId",
  VACCINATION_DOSE: "app.vaccination.dose",

  ELIGIBILITY_CRITERIA: "app.eligibility.criteria",
  ELIGIBILITY_YEAR_OF_BIRTH: "app.eligibility.yearOfBirth",

  VERIFY_MOBILE: "app.verify.mobile",
  VERIFY_ENTER_MOBILE: "app.verify.enterMobile",
  VERIFY_CONTINUE: "app.verify.continue",
  OTP_ENTER_OTP: "app.otp.enterOtp",
  OTP_VERIFY: "app.otp.verify",
  BENEFICIARY_FORM_VALIDATION: "app.beneficiary.validation",
  BENEFICIARY_ID_DETAILS: "app.beneficiary.id_details",
  BENEFICIARY_NAME: "app.beneficiary.name",
  BENEFICIARY_ENTER_NAME: "app.beneficiary.enterName",
  BENEFICIARY_ID_TYPE: "app.beneficiary.idType",
  BENEFICIARY_SELECT_ID_TYPE: "app.beneficiary.selectIdType",
  BENEFICIARY_ID_TYPES_LIST_ONE: "app.beneficiary.in.gov.kebele",
  BENEFICIARY_ID_TYPES_LIST_TWO: "app.beneficiary.in.gov.workid",
  BENEFICIARY_ID_TYPES_LIST_THREE: "app.beneficiary.in.gov.drivinglicense",
  BENEFICIARY_ID_TYPES_LIST_FOUR: "app.beneficiary.in.gov.passport",
  BENEFICIARY_ID_TYPES_LIST_FIVE: "app.beneficiary.in.gov.other",
  BENEFICIARY_ID_NUMBER: "app.beneficiary.idNumber",
  BENEFICIARY_ENTER_ID_NUMBER: "app.beneficiary.enterIdNumber",
  BENEFICIARY_AGE: "app.beneficiary.age",
  BENEFICIARY_YEARS: "app.beneficiary.years",
  BENEFICIARY_GENDER: "app.beneficiary.gender",
  BENEFICIARY_SELECT_GENDER: "app.beneficiary.selectGender",
  BENEFICIARY_GENDERS_LIST_ONE: "app.beneficiary.gendersListOne",
  BENEFICIARY_GENDERS_LIST_TWO: "app.beneficiary.gendersListTwo",
  BENEFICIARY_RESIDENCE_DETAILS: "app.beneficiary.residenceDetails",
  BENEFICIARY_REGION: "app.beneficiary.region",
  BENEFICIARY_SELECT_REGION: "app.beneficiary.selectRegion",
  BENEFICIARY_SUBCITY_ZONE: "app.beneficiary.subcityZone",
  BENEFICIARY_SELECT_SUBCITY_ZONE: "app.beneficiary.selectSubcityZone",
  BENEFICIARY_DISTRICTS_LIST: "app.beneficiary.districtsList",
  BENEFICIARY_WOREDA: "app.beneficiary.woreda",
  BENEFICIARY_ENTER_YOUR_WOREDA: "app.beneficiary.enterYourWoreda",
  BENEFICIARY_OCCUPATION: "app.beneficiary.occupation",
  BENEFICIARY_SELECT_OCCUPATION: "app.beneficiary.selectOccupation",
  BENEFICIARY_OCCUPATIONS_LIST_ONE: "app.beneficiary.occupationsListOne",
  BENEFICIARY_OCCUPATIONS_LIST_TWO: "app.beneficiary.occupationsListTwo",
  BENEFICIARY_NATIONALITY: "app.beneficiary.nationality",
  BENEFICIARY_SELECT_NATIONALITY: "app.beneficiary.selectNationality",
  BENEFICIARY_NATIONALITY_ETHIO: "app.beneficiary.nationalityEthio",
  BENEFICIARY_NATIONALITY_OTHER: "app.beneficiary.nationalityOther",
  BENEFICIARY_MEDICAL_INFO: "app.beneficiary.medicalInfo",
  BENEFICIARY_COMORBID_CONDITIONS: "app.beneficiary.comorbidConditions",
  BENEFICIARY_ENTER_COMORBID_CONDITIONS:
    "app.beneficiary.enterComorbidConditions",
  BENEFICIARY_VITAL_SIGNS: "app.beneficiary.vitalSigns",
  BENEFICIARY_PULSE_RATE: "app.beneficiary.pulseRate",
  BENEFICIARY_RESPIRATORY_RATE: "app.beneficiary.respiratoryRate",
  BENEFICIARY_BLOOD_PRESSURE: "app.beneficiary.bloodPressure",
  BENEFICIARY_TEMPERATURE: "app.beneficiary.temperature",
  BENEFICIARY_TEMPERATURE_PLACE_HOLDER:
    "app.beneficiary.temperaturePlaceHolder",
  BENEFICIARY_COMORBIDITIES: "app.beneficiary.comorbidities",
  BENEFICIARY_CONTACT_INFORMATION: "app.beneficiary.contactInformation",
  BENEFICIARY_MOBILE: "app.beneficiary.mobile",
  BENEFICIARY_AGE_ERROR_MSG: "app.beneficiary.ageError",
  BENEFICIARY_MOBILE_ERROR_MSG: "app.beneficiary.mobileError",
  BENEFICIARY_NATIONAL_ID_TYPE_ERROR_MSG: "app.beneficiary.nationalIdTypeError",
  BENEFICIARY_NATIONAL_ID_ERROR_MSG: "app.beneficiary.nationalIdNumberError",
  BENEFICIARY_NAME_ERROR_MSG: "app.beneficiary.nameError",
  BENEFICIARY_INVALID_NAME_ERR_MSG: "app.beneficiary.invalidNameError",
  BENEFICIARY_MINIMUM_LENGTH_OF_NAME_ERROR_MSG:
    "app.beneficiary.minLenNameError",
  BENEFICIARY_MAXIMUM_LENGTH_OF_NAME_ERROR_MSG:
    "app.beneficiary.maxLenNameError",
  BENEFICIARY_STATE_ERROR_MSG: "app.beneficiary.stateError",
  BENEFICIARY_DISTRICT_ERROR_MSG: "app.beneficiary.districtError",
  BENEFICIARY_NATIONALITY_ERROR_MSG: "app.beneficiary.nationalityError",
  BENEFICIARY_GENDER_ERROR_MSG: "app.beneficiary.genderError",
  BENEFICIARY_ERROR_ID_MESSAGE: "app.beneficiary.errorId",
  BENEFICIARY_EMAIL_ERROR_MESSAGE: "app.beneficiary.emailError",
  BENEFICIARY_REGISTRATION_DATE:"app.beneficiary.registrationDate",
  BENEFICIARY_PINCODE_ERROR_MESSAGE: "app.beneficiary.pincodeError",
  BENEFICIARY_PREGNANT:"app.beneficiary.pregnant",
  BENEFICIARY_LACTATING:"app.beneficiary.lactating",
  BENEFICIARY_RELATIVE_NAME:"app.beneficiary.relativeName",
  BENEFICIARY_RELATIVE_PHONE_NUMBER:"app.beneficiary.relativePhoneNumber",
  BENEFICIARY_HEIGHT:"app.beneficiary.height",
  BENEFICIARY_WEIGHT:"app.beneficiary.weight",
  BENEFICIARY_ON_MEDICATION:"app.beneficiary.onMedication",
  BENEFICIARY_ALERGY:"app.beneficiary.alergy",
  BENEFICIARY_VACCINES_FOR_SIX_MONTHS:"app.beneficiary.receivedMedicationForSixMonths",
  BENEFICIARY_SUBSTANCE_ABUSE:"app.beneficiary.substanceAbuse",
  BENEFICIARY_SPECIFY:"app.beneficiary.specify",
  BENEFICIARY_RADIO_YES:"app.beneficiary.radioYes",
  BENEFICIARY_RADIO_NO:"app.beneficiary.radioNo",
  BENEFICIARY_COMO_HYPER:"app.beneficiary.comoHyper",
  BENEFICIARY_COMO_ASTHMA:"app.beneficiary.comoAsthma",
  BENEFICIARY_COMO_HIV:"app.beneficiary.comoHiv",
  BENEFICIARY_COMO_KIDNEY:"app.beneficiary.comoKidney",
  BENEFICIARY_COMO_DIABETES:"app.beneficiary.comoDiabetes",
  BENEFICIARY_COMO_CARDIAC:"app.beneficiary.comoCardiac",
  BENEFICIARY_COMO_EPILEPSY:"app.beneficiary.comoEpilepsy",
  BENEFICIARY_CONSCIOUSNESS_LEVEL:"app.beneficiary.consciousnessLevel",
  BENEFICIARY_SPO_TWO:"app.beneficiary.spoTwo",
  BENEFICIARY_BLOOD_PRESSURE_SYSTOLIC: "app.beneficiary.bloodPressureSystolic",
  BENEFICIARY_BLOOD_PRESSURE_DIASTOLIC: "app.beneficiary.bloodPressureDiastolic",
});
