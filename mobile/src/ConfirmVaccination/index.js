import React, {createContext, useContext, useEffect, useMemo, useReducer, useState} from "react";
import "./index.scss"
import {BaseFormCard} from "../components/BaseFormCard";
import {Redirect, useHistory} from "react-router";
import {SelectVaccinator} from "../components/SelectVaccinator";
import {CONSTANT} from "../utils/constants";
import {appIndexDb} from "../AppDatabase";
import config from "config.js"
import {programDb} from "../Services/ProgramDB";
import {getSelectedProgram} from "../components/ProgramSelection";
import {getVaccinationDetails, saveVaccinationDetails, saveVaccinationDetailsInfant} from "../utils/storage";
import {getMessageComponent, LANGUAGE_KEYS, useLocale} from "../lang/LocaleContext";
import infant_vaccinator_schema from '../jsonSchema/immunization_form_infant.json';
import Form from "@rjsf/core/lib/components/Form";
import {Button} from "react-bootstrap";
import {SyncFacade} from "../SyncFacade";

export function ConfirmFlow(props) {
    return (
        <ConfirmVaccineProvider>
            <ConfirmVaccination {...props}/>
        </ConfirmVaccineProvider>
    );
}

export function ConfirmVaccination(props) {
    const {pageName, recipient_id} = props.match.params;

    function getForm() {
        switch (pageName) {
            case CONSTANT.SELECT_VACCINATOR:
                return (getSelectedProgram() == CONSTANT.INFANT_ROUTINE ? <WalkEnrollmentForm enrollCode={recipient_id}/> : <SelectVaccinator enrollCode={recipient_id}/>);
            default:
                return <Redirect to={config.urlPath + '/queue'}/>
        }
    }

    return (
        <div className="confirm-vaccination-container">
            <BaseFormCard title={getMessageComponent(LANGUAGE_KEYS.VACCINATION_TITLE)}>
                <div className="pt-3 form-wrapper">
                    {
                        getForm()
                    }
                </div>
                {

                }
            </BaseFormCard>
        </div>
    );
}


const ConfirmVaccineContext = createContext(null)

export function ConfirmVaccineProvider(props) {
    const [state, dispatch] = useReducer(confirmVaccineReducer, initialState)
    const value = useMemo(() => [state, dispatch], [state])
    return <ConfirmVaccineContext.Provider value={value} {...props} />
}

const initialState = {};

function confirmVaccineReducer(state, action) {
    switch (action.type) {
        case ACTION_PATIENT_COMPLETED: {
            const newState = {...state}
            newState.enrollCode = action.payload.enrollCode;
            newState.vaccinatorId = action.payload.vaccinatorId;
            newState.medicineId = action.payload.medicineId;
            newState.batchCode = action.payload.batchCode
            return newState
        }
        default:
            throw new Error();
    }
}

function WalkEnrollmentForm(props) {
    const {markPatientCompleteInfant, goNext} = useConfirmVaccine();
    const {getText, selectLanguage} = useLocale()
    const [vaccinatorSchema, setVaccinatorSchema] = useState(infant_vaccinator_schema);
    const [formData, setFormData] = useState({});
    const [isFormTranslated, setFormTranslated] = useState(false);

    useEffect(() => {
        for (let property in vaccinatorSchema.properties) {
            if (vaccinatorSchema.properties.hasOwnProperty(property)) {
                const labelText = getText("app.enrollment." + property);
                vaccinatorSchema.properties[property].title = labelText
            }
        }
        for (let property in vaccinatorSchema.properties.antigens.properties) {
            if (vaccinatorSchema.properties.antigens.properties.hasOwnProperty(property)) {
                const labelText = getText("app.enrollment." + property);
                vaccinatorSchema.properties.antigens.properties[property].title = labelText
            }
        }
        for (let property in vaccinatorSchema.properties.userImmunization.properties) {
            if (vaccinatorSchema.properties.userImmunization.properties.hasOwnProperty(property)) {
                const labelText = getText("app.enrollment." + property);
                vaccinatorSchema.properties.userImmunization.properties[property].title = labelText
            }
        }
        setVaccinatorSchema(vaccinatorSchema)
        setFormTranslated(true)

    }, [selectLanguage]);

    const customFormats = {
        'phone-in': /\(?\d{3}\)?[\s-]?\d{3}[\s-]?\d{4}$/
    };

    const uiSchema = {
        classNames: "form-container",
    };

    function onActionBtnClick(formData) {
        const payload = {
            enrollCode: props.enrollCode,
            ...formData
        }
        markPatientCompleteInfant(payload).then((value) => {
            return SyncFacade.push()
        }).then((value => {
            goNext(ACTION_PATIENT_COMPLETED, `/queue`, {});
        })).catch((e) => {
            goNext(ACTION_PATIENT_COMPLETED, `/queue`, {});
        })
    }

    return (<Form
                key={isFormTranslated}
                schema={vaccinatorSchema}
                customFormats={customFormats}
                uiSchema={uiSchema}
                formData={formData}
                onSubmit={(e) => {
                    onActionBtnClick(e.formData)
                    // goNext(FORM_WALK_IN_ENROLL_FORM, FORM_WALK_IN_ENROLL_CONFIRMATION, {...state, ...e.formData});
                }}
            >
                <Button type={"submit"} variant="outline-primary"
                        className="action-btn">{getMessageComponent(LANGUAGE_KEYS.BUTTON_NEXT)}</Button>
            </Form>
    );
}


export const ACTION_PATIENT_COMPLETED = "patientCompleted"

export function useConfirmVaccine() {

    const context = useContext(ConfirmVaccineContext)
    const history = useHistory();
    if (!context) {
        throw new Error(`useConfirmVaccine must be used within a ConfirmVaccineProvider`)
    }
    const [state, dispatch] = context;

    const goNext = function (current, next, payload) {
        payload.currentForm = current;
        dispatch({type: current, payload: payload})
        if (next) {
            history.push(config.urlPath + next)
        }
    }

    const goBack = function () {
        history.goBack()
    }

    const getFormDetails = async function () {
        const selectedProgram = getSelectedProgram();
        const vaccinator = await programDb.getVaccinators();
        const medicines = await programDb.getMedicines(selectedProgram);
        const vaccines = await programDb.getVaccines();
        const batchIds = await programDb.getBatches();
        const vaccinationDetails = getVaccinationDetails();
        return {
            vaccinator: vaccinator || [],
            selectedVaccinator: vaccinationDetails.vaccinatorId,
            medicines: medicines || [],
            vaccines: vaccines || [],
            batchIds: batchIds || [],
            selectedMedicine: vaccinationDetails.medicineId,
            selectedDose: vaccinationDetails.selectedDose,
            selectedBatchId: vaccinationDetails.lastBatchId,
        }
    }

    const markPatientComplete = async function (payload) {
        saveVaccinationDetails(payload)
        try {
            await appIndexDb.saveEvent(payload)
            await appIndexDb.markPatientAsComplete(payload)
        } catch (e) {
            return Promise.reject(e.message)
        }
    }

    const markPatientCompleteInfant = async function (payload) {
        saveVaccinationDetailsInfant(payload)
        try {
            await appIndexDb.saveEvent(payload)
            await appIndexDb.markPatientAsComplete(payload)
        } catch (e) {
            return Promise.reject(e.message)
        }
    }

    return {
        state,
        dispatch,
        goNext,
        goBack,
        markPatientComplete,
        markPatientCompleteInfant,
        getFormDetails
    }
}
