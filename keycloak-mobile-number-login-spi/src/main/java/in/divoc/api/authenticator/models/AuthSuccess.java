package in.divoc.api.authenticator.models;

public class AuthSuccess {
    private AccessToken accessToken;
    private String refreshToken;

    public AuthSuccess(AccessToken accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public class AccessToken {
        private String accessTokenString ;

        public AccessToken(String accessTokenString) {
            this.accessTokenString = accessTokenString;
        }

        public String getAccessTokenString() {
            return accessTokenString;
        }
    }

    

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}